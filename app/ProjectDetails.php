<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectDetails extends Model
{
    protected $fillable = [
        'project_title',
        'scope',
        'description',
        'language',
        'user_id',
        'supervisor_type',
        'supervisor_name',
        'position',
        'organization'
    ];

    public function setCategoryAttribute($value)
    {
        $this->attributes['language'] = json_encode($value);
    }

    public function getCategoryAttribute($value)
    {
        return $this->attributes['language'] = json_decode($value);
    }

}
