<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcadamicYear extends Model
{
    protected $fillable = [
        'acadamicyear'
    ];
}
