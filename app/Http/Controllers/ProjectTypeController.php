<?php

namespace App\Http\Controllers;

use App\ProjectType;
use Illuminate\Http\Request;

class ProjectTypeController extends Controller
{

    public function index(Request $request)
    {
        $data = ProjectType::orderBy('id','ASC')->get();
        return view('backend.projecttype.index', compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('backend.projecttype.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'projecttype' => 'required'
        ]);

        $input = $request->all();
        $projecttype = ProjectType::create($input);

        toastr()->success('Data Created Successfully!');

        return redirect()->route('projecttype.index')
                        ->with('success','Project Type stored');
    }

    public function show(ProjectType $projectType)
    {
        
    }

    public function edit($id)
    {
        $project = ProjectType::find($id);
        return view('backend.projecttype.edit',compact('project'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'projecttype' => 'required'
        ]);

        $input = $request->all();
        $projectType = ProjectType::find($id);
        $projectType->update($input);

        toastr()->success('Data Updated Successfuly!');

        return redirect()->route('projecttype.index')
                        ->with('success','Project Type updated successfully');
    }

    public function destroy($id)
    {
        ProjectType::find($id)->delete();

        toastr()->success('Data Deleted Successfully!');
        return redirect()->route('projecttype.index')
                        ->with('success','Deleted successfully');
    }
}
