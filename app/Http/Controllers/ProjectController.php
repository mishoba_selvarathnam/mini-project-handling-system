<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectDetails;
use App\ProjectType;
use Illuminate\Http\Request;
use Auth;
use Response;
use Redirect;
use App\AcadamicYear;
use App\User;
use Notification;
use App\Notifications\ProjectAdded;


class ProjectController extends Controller
{

    public function index()
    {
        $acadamic = AcadamicYear::all();
        $details = ProjectDetails::where('user_id',Auth::user()->id)->first();
        $project = Project::where('user_id',Auth::user()->id)->get();
        if($details == Null) {
            toastr()->error('Please update your project details before submit a project!');
            return Redirect::to('/project/AddDetails');
        } else {

            return view('backend.student.projects.index', compact('details', 'project', 'acadamic'));
        }
    }

    public function create_document()
    {
        $projecttype = ProjectType::all();
        $acadamic = AcadamicYear::all();
        return view('backend.student.projects.create', compact('projecttype', 'acadamic'));
    }

    public function store_document(Request $request)
    {
        $this->validate($request, [
            'projecttype' => 'required',
            'project' => 'required',
            'saveas' => 'required'
        ]);

        $input = $request->all();

        $name = $input['saveas'];

        $fileName = $name.'.'.$request->project->getClientOriginalExtension();
        $request->project->move(public_path('uploads'), $fileName);

        $input['user_id'] = Auth::user()->id;
        $input['project'] = $fileName;
        $project = Project::create($input);

  
        $details = [
            'greeting' => 'Hi,',
            'body' => 'Your Project was successfully uploaded',
            'thanks' => 'Thank you for using Mini Handler!',
            'actionText' => 'View the site to check your project!',
            'actionURL' => url('/'),
            'user_id' => Auth::user()->id
        ];
  
        Auth::user()->notify(new ProjectAdded($details));

        toastr()->success('Project Doument Save Successfully!');
        return redirect()->route('project.index')
                        ->with('success','Project created successfully');
    }

    public function create_details()
    {
        $acadamic = AcadamicYear::all();
        $supervisor = User::where('role', 'supervisor')->get();
        $coordinator = User::where('role', 'coordinator')->get();
        return view('backend.student.projects.create_details', compact('acadamic', 'supervisor', 'coordinator'));
    }

    public function store_details(Request $request)
    {
        $this->validate($request, [
            'project_title' => 'required',
            'scope' => 'required',
            'description' => 'required',
            'language' => 'required'
        ]);

        $input = $request->all();
        $language = $request->input('language');
        $language = implode(',', $language);
        $input['language'] = $language;
        $input['user_id'] = Auth::user()->id;

        $details = ProjectDetails::create($input);

        toastr()->success('Project Details Saved Successfully!');
        return redirect()->route('project.index')
                        ->with('success', 'Your Project Details Added Successfully');
    }

    public function edit($id)
    {
        $acadamic = AcadamicYear::all();
        $project = Project::find($id);
        $projecttype = ProjectType::all();
        return view('backend.student.projects.edit', compact('project', 'projecttype', 'acadamic') );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'projecttype' => 'required',
            'project' => 'required',
            'saveas' => 'required'
        ]);

        $input = $request->all();

        $name = $input['saveas'];

        $fileName = $name.'.'.$request->project->getClientOriginalExtension();
        $request->project->move(public_path('uploads'), $fileName);

        $input['user_id'] = Auth::user()->id;
        $input['project'] = $fileName;

        $user = Project::find($id);
        $user->update($input);

        toastr()->success('Project Document Updated Successfully!');
        return redirect()->route('project.index')
                        ->with('success', 'Your Project Document Updated Successfully');
    }

    public function show(Project $project)
    {
        //
    }

    public function edit_details($id)
    {
        $acadamic = AcadamicYear::all();
        $details = ProjectDetails::find($id);
        $supervisor = User::where('role', 'supervisor')->get();
        $coordinator = User::where('role', 'coordinator')->get();
        return view('backend.student.projects.edit_details', compact('details', 'acadamic', 'supervisor', 'coordinator') );
    }

    public function update_details(Request $request, $id)
    {
        $this->validate($request, [
            'project_title' => 'required',
            'scope' => 'required',
            'description' => 'required',
            'language' => 'required'
        ]);

        $input = $request->all();
        $language = $request->input('language');
        $language = implode(',', $language);
        $input['language'] = $language;

        $user = ProjectDetails::find($id);
        $user->update($input);

        toastr()->success('Project Details Updated Successfully!');
        return redirect()->route('project.index')
                        ->with('success', 'Your Project Details Updated Successfully');
    }

    public function destroy(Project $project)
    {
        //
    }

    public function view($id){
        $file = Project::find($id)->project;
        $pathToFile = public_path().'/uploads/'.$file;
        return response()->file($pathToFile);
      }
}
