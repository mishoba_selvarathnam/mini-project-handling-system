<?php

namespace App\Http\Controllers;

use App\Coordinator;
use Redirect;
use Session;
use DB;
use App\AcadamicYear;
use Auth;
use App\ProjectDetails;
use App\Project;
use App\User;
use Illuminate\Http\Request;

use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class CoordinatorController extends Controller
{

    public function index()
    {
        $acadamic = AcadamicYear::all();
        toastr()->success('Welcome back to your dashboard!');
        return view('backend.coordinator.layouts.home', compact('acadamic'));
    }

    public function project(Request $request)
    {
        $acadamic = AcadamicYear::all();

        $properties = DB::table('users')
                   ->join('project_details','project_details.user_id','=','users.id')
                   ->select('users.*','project_details.supervisor_name')
                   ->where('supervisor_name', Auth::user()->username)
                   ->get();

        $groups = $properties->groupBy('acadamicyear');

        return view('backend.coordinator.Project.index', compact('acadamic', 'properties', 'groups'))
                            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function show(Request $request,$id)
    {
        $user = User::find($id);
        $details = ProjectDetails::where('user_id', $user->id)->first();
        $project = Project::where('user_id', $user->id)->get();

        return view('backend.coordinator.Project.show', compact('user', 'details', 'project'));
    }

    public function all_project(Request $request)
    {
        $properties = DB::table('users')
                   ->join('project_details','project_details.user_id','=','users.id')
                   ->select('users.*','project_details.user_id')
                   ->get();

        $groups = $properties->groupBy('acadamicyear');

        return view('backend.coordinator.All.index', compact('groups', 'properties'))
                            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function show_project(Request $request,$id)
    {
        $user = User::find($id);
        $details = ProjectDetails::where('user_id', $user->id)->first();
        $project = Project::where('user_id', $user->id)->get();

        return view('backend.coordinator.All.show', compact('user', 'details', 'project'));
    }

    public function view($id){
        $file = Project::find($id)->project;
        $pathToFile = public_path().'/uploads/'.$file;
        return response()->file($pathToFile);
      }

      public function changecoordinatorpassword()
      {
          return view('backend.coordinator.change_coordinator_password');
      }
  
      public function storecoordinatorpassword(Request $request)
      {
          $request->validate([
              'current_password' => ['required', new MatchOldPassword],
              'new_password' => ['required'],
              'new_confirm_password' => ['same:new_password'],
          ]);
     
          User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
  
          toastr()->success('You have Successfully updated your password!');
     
          return redirect()->back();
      }

}
