<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use Hash;
use App\AcadamicYear;
use App\Http\Requests;
use Image;
use Auth;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->get();
        return view('backend.users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function student_index(Request $request)
    {

        $properties = DB::table('users')
                   ->select('users.*')
                   ->where('role','=', 'student')
                   ->get();

       $groups = $properties->groupBy('acadamicyear');

        return view('backend.users.student.index',compact('properties', 'groups'))
                       ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function supervisor_index(Request $request)
    {
        $data = User::where('role', 'supervisor')->get();
        return view('backend.users.supervisor.index',compact('data'))
                       ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function coordinator_index(Request $request)
    {
        $data = User::where('role', 'coordinator')->get();
        return view('backend.users.coordinator.index',compact('data'))
                       ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create_student()
    {
        $acadamic = AcadamicYear::all();
        return view('backend.users.student.create', compact('acadamic'));
    }

    public function store_student(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'acadamicyear' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['role'] = 'student';
        $user = User::create($input);

        toastr()->success('New Student Created Successfully!');
        return redirect()->route('users.student_index')
                        ->with('success','Student created successfully');
    }

    public function create_supervisor()
    {
        return view('backend.users.supervisor.create');
    }

    public function store_supervisor(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['role'] = 'supervisor';
        $user = User::create($input);

        toastr()->success('New User Created Successfully!');
        return redirect()->route('users.supervisor_index')
                        ->with('success','User created successfully');
    }

    public function create_coordinator()
    {
        return view('backend.users.coordinator.create');
    }

    public function store_coordinator(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['role'] = 'coordinator';
        $user = User::create($input);

        toastr()->success('New User Created Successfully!');
        return redirect()->route('users.coordinator_index')
                        ->with('success','Coordinator created successfully');
    }

    public function coordinator_role($id)
    {
        $user = User::find($id);
        return view('backend.users.coordinator.coordinator_role',compact('user'));
    }

    public function update_coordinator_role(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required',
        ]);

        $input = $request->all();
        $user = User::find($id);
        $user->update($input);

        toastr()->success('User Role Update Successfully!');

        return redirect()->route('users.coordinator_index')
                        ->with('success','Coordinator Role Update Successfully');
    }
    public function supervisor_role($id)
    {
        $user = User::find($id);
        return view('backend.users.supervisor.supervisor_role',compact('user'));
    }

    public function update_supervisor_role(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required',
        ]);

        $input = $request->all();
        $user = User::find($id);
        $user->update($input);

        toastr()->success('User Role Updated Successfully!');

        return redirect()->route('users.supervisor_index')
                        ->with('success','Supervisor Role Update Successfully');
    }

    public function edit_student($id)
    {
        $user = User::find($id);
        $acadamic = AcadamicYear::all();
        return view('backend.users.student.edit',compact('user', 'acadamic'));
    }

    public function update_student(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required',
            'acadamicyear'=> 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::find($id);
        $user->update($input);

        toastr()->success('Student Details Updated Successfully!');

        return redirect()->route('users.student_index')
                        ->with('success','Student updated successfully');
    }

    public function edit_coordinator($id)
    {
        $user = User::find($id);
        return view('backend.users.coordinator.edit',compact('user'));
    }

    public function update_coordinator(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::find($id);
        $user->update($input);

        toastr()->success('User Details Updated Successfully!');

        return redirect()->route('users.coordinator_index')
                        ->with('success','Coordinator details updated successfully');
    }

    public function edit_supervisor($id)
    {
        $user = User::find($id);
        return view('backend.users.supervisor.edit',compact('user'));
    }

    public function update_supervisor(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::find($id);
        $user->update($input);

        toastr()->success('User Details Updated Successfully!');

        return redirect()->route('users.supervisor_index')
                        ->with('success','Supervisor details updated successfully');
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        toastr()->success('User Deleted Successfully!');
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
    public function destroy_student($id)
    {
        User::find($id)->delete();
        toastr()->success('User Deleted Successfully!');
        return redirect()->route('users.student_index')
                        ->with('success','Student deleted successfully');
    }
    public function destroy_supervisor($id)
    {
        User::find($id)->delete();
        toastr()->success('User Deleted Successfully!');
        return redirect()->route('users.supervisor_index')
                        ->with('success','User deleted successfully');
    }
    public function destroy_coordinator($id)
    {
        User::find($id)->delete();
        toastr()->success('User Deleted Successfully!');
        return redirect()->route('users.coordinator_index')
                        ->with('success','Coordinator deleted successfully');
    }


}