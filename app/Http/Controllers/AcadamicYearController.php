<?php

namespace App\Http\Controllers;

use App\AcadamicYear;
use Illuminate\Http\Request;

class AcadamicYearController extends Controller
{

    public function index(Request $request)
    {
        $data = AcadamicYear::orderBy('id','ASC')->get();
            return view('backend.acadamic.index',compact('data'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('backend.acadamic.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'acadamicyear' => 'required'
        ]);

        $input = $request->all();
        $acadamic = AcadamicYear::create($input);

        toastr()->success('Acadamic Year created successfully!');

        return redirect()->route('acadamic.index')
                        ->with('success','Created Successfully');
    }

    public function show(AcadamicYear $acadamicYear)
    {
        
    }

    public function edit($id)
    {
        $acadamic = AcadamicYear::find($id);
        return view('backend.acadamic.edit', compact('acadamic'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'acadamicyear' => 'required'
        ]);

        $input = $request->all();
        $acadamic = AcadamicYear::find($id);
        $acadamic->update($input);

        toastr()->success('Updated Successfully!');

        return redirect()->route('acadamic.index')
                        ->with('success', 'Year Updated Successfully');

    }

    public function destroy($id)
    {
        AcadamicYear::find($id)->delete();
        toastr()->success('Data Deleted Successfully!');
        return redirect()->route('acadamic.index')
                        ->with('success', 'Deletetd Successfully');
    }
}
