<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginController extends Controller
{
    use AuthenticatesUsers;


    public function redirectTo(){
        switch(Auth::user()->role){
         case 'admin':
             $this->redirectTo = '/admin';
         return $this->redirectTo;
         break;

         case 'student':
         $this->redirectTo = '/student';
         return $this->redirectTo;
         break;

         case 'supervisor':
         $this->redirectTo = '/supervisor';
         return $this->redirectTo;
         break;

         default:
         $this->redirectTo = '/coordinator';
         return $this->redirectTo;
        }
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

}
