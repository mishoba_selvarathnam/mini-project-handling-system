<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use App\Student;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Image;
use Storage;
use App\AcadamicYear;
use App\Rules\MatchOldPassword;
use DB;


class StudentController extends Controller
{
    public function index()
    {
        $acadamic = AcadamicYear::all();
        toastr()->success('Welcome back to your dashboard!');
        return view('backend.student.layouts.home', compact('acadamic'));
    }

    public function profile(Request $request)
    {
        $acadamic = AcadamicYear::all();
        $data = User::where('id' , Auth::user()->id)->first();
        return view('backend.student.profile.index', compact('data', 'acadamic') );
    }

    public function project(Request $request)
    {
        $acadamic = AcadamicYear::all();

        $properties = DB::table('users')
                   ->join('project_details','project_details.user_id','=','users.id')
                   ->select('users.*','project_details.project_title', 'project_details.description', 'project_details.language')
                   ->get();

       $groups = $properties->groupBy('acadamicyear');

        return view('backend.student.projects.all_project', compact('acadamic', 'properties', 'groups'))
                            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function show()
    {
        
    }

    public function edit_avatar($id)
    {
        $acadamic = AcadamicYear::all();
        return view('backend.student.profile.edit', compact('acadamic'), array('user' => Auth::user()));
    }

    public function update_avatar(Request $request, $id)
    {
        $this->validate($request, [
            'avatar' => 'required',
        ]);

        $product = User::find($id);
        $oldavatar = '/storage/photo/'.$product->avatar;

        if($request->hasFile('avatar'))
        {
            
            Storage::delete('oldavatar');
            $avatar = $request->file('avatar');

            $avatarSaveAsName = "avatar.".time(). '.' . $avatar->getClientOriginalExtension();

            $avatar_upload_path = 'photo';
            $avatar_url = $avatarSaveAsName;
            $success = $avatar->move($avatar_upload_path, $avatarSaveAsName);
        }

        $input = $request->all();
        $input['avatar'] = $avatar_url;

        
        $product = User::find($id);
        $product->update($input);

        toastr()->success('Profile Picture Update Successfully');

        return redirect()->route('student.profile');
    }

    public function edit_details($id)
    {
        $acadamic = AcadamicYear::all();

        return view('backend.student.profile.edit_details',compact('acadamic'), array('user' => Auth::user()));
    }

    public function update_name(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required'
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->update($input);

        echo "success";

        toastr()->success('your Details Updated Successfully!');


        return redirect()->route('student.profile');
    }

    public function changestudentpassword()
    {
        return view('backend.student.change_student_password');
    }

    public function storestudentpassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        toastr()->success('You have Successfully updated your password!');
   
        return redirect()->back();
    }

}