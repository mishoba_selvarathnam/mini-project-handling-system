<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Admin
{

    public function handle($request, Closure $next)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->role == 'admin') {
            return $next($request);
        }

        if (Auth::user()->role == 'student') {
            redirect()->route('student');
        }

        if (Auth::user()->role == 'supervisor') {
            return redirect()->route('supervisor');
        }

        if (Auth::user()->role == 'coordinator') {
            return redirect()->route('coordinator');
        }
    }
}
