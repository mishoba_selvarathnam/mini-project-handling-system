<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class Coordinator
{

    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->role == 'admin') {
            redirect()->route('admin');
        }

        if (Auth::user()->role == 'student') {
            redirect()->route('student');
        }

        if (Auth::user()->role == 'supervisor') {
            return redirect()->route('supervisor');
        }

        if (Auth::user()->role == 'coordinator') {
            return $next($request);
        }
    }
}
