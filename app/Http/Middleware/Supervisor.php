<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Supervisor
{
    public function handle($request, Closure $next)
    {
        return $next($request);

        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->role == 'admin') {
            redirect()->route('admin');
        }

        if (Auth::user()->role == 'student') {
            redirect()->route('student');
        }

        if (Auth::user()->role == 'coordinator') {
            redirect()->route('coordinator');
        }

        if (Auth::user()->role == 'supervisor') {
            redirect()->route('supervisor');
        }
    }
}
