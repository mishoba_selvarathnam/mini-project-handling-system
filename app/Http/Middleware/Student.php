<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Student
{

    public function handle($request, Closure $next)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->role == 'admin') {
            redirect()->route('admin');
        }

        if (Auth::user()->role == 'student') {
            return $next($request);
        }

        if (Auth::user()->role == 'supervisor') {
            return redirect()->route('supervisor');
        }

        if (Auth::user()->role == 'coordinator') {
            redirect()->route('coordinator');
        }
    }
}
