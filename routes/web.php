<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::get('/admin', 'AdminController@index');
    Route::resource('users','UserController');
    Route::resource('acadamic', 'AcadamicYearController');
    Route::resource('projecttype', 'ProjectTypeController');

    Route::get('student_index','UserController@student_index')->name('users.student_index');
    Route::get('create_student','UserController@create_student')->name('users.create_student');
    Route::post('create_student','UserController@store_student')->name('users.store_student');
    Route::get('edit_student/{id}','UserController@edit_student')->name('users.edit_student');
    Route::post('edit_student/{id}','UserController@update_student')->name('users.update_student');
    Route::delete('destroy_student/{id}','UserController@destroy_student')->name('users.destroy_student');

    Route::get('supervisor_index','UserController@supervisor_index')->name('users.supervisor_index');
    Route::get('create_supervisor','UserController@create_supervisor')->name('users.create_supervisor');
    Route::post('create_supervisor','UserController@store_supervisor')->name('users.store_supervisor');
    Route::get('edit_supervisor/{id}','UserController@edit_supervisor')->name('users.edit_supervisor');
    Route::post('edit_supervisor/{id}','UserController@update_supervisor')->name('users.update_supervisor');
    Route::get('supervisor_role/{id}','UserController@supervisor_role')->name('users.supervisor_role');
    Route::patch('update_supervisor_role/{id}','UserController@update_supervisor_role')->name('users.update_supervisor_role');
    Route::delete('destroy_supervisor/{id}','UserController@destroy_supervisor')->name('users.destroy_supervisor');

    Route::get('coordinator_index','UserController@coordinator_index')->name('users.coordinator_index');
    Route::get('create_coordinator','UserController@create_coordinator')->name('users.create_coordinator');
    Route::post('create_coordinator','UserController@store_coordinator')->name('users.store_coordinator');
    Route::get('edit_coordinator/{id}','UserController@edit_coordinator')->name('users.edit_coordinator');
    Route::post('edit_coordinator/{id}','UserController@update_coordinator')->name('users.update_coordinator');
    Route::get('coordinator_role/{id}','UserController@coordinator_role')->name('users.coordinator_role');
    Route::patch('update_coordinator_role{id}','UserController@update_coordinator_role')->name('users.update_coordinator_role');
    Route::delete('destroy_coordinator/{id}','UserController@destroy_coordinator')->name('users.destroy_coordinator');

    Route::get('change-password', 'AdminController@changepassword')->name('password_change');
    Route::post('change-password', 'AdminController@store')->name('change.password');

    Route::get('student/changepassword', 'StudentController@changestudentpassword')->name('student.change_password');
    Route::post('student/changepassword', 'StudentController@storestudentpassword')->name('change.student.password');

    Route::get('supervisor/updatepassword', 'SupervisorController@changesupervisorpassword')->name('supervisor.password');
    Route::post('supervisor/updatepassword', 'SupervisorController@storesupervisorpassword')->name('supervisor.change_password');

    Route::get('coordinator/passwordchange', 'CoordinatorController@changecoordinatorpassword')->name('coordinator.passwordchange');
    Route::post('coordinator/passwordchange', 'CoordinatorController@storecoordinatorpassword')->name('coordinator.changepassword');

});

Route::get('/student', 'StudentController@index')->name('student.index')->middleware('student');

    

Route::get('profile', 'StudentController@profile')->name('student.profile');
Route::get('profile/edit_avatar/{id}', 'StudentController@edit_avatar')->name('profile.edit');
Route::post('profile/edit_avatar/{id}', 'StudentController@update_avatar')->name('profile.update');
Route::get('profile/edit_details/{id}', 'StudentController@edit_details')->name('profile.edit_details');
Route::post('profile/edit_details/{id}', 'StudentController@update_name')->name('profile.update_name');
Route::get('studentproject', 'StudentController@project')->name('all_project.index');

Route::get('project', 'ProjectController@index')->name('project.index');
Route::get('project/AddDetails','ProjectController@create_details')->name('project.create_details');
Route::post('project/AddDetails','ProjectController@store_details')->name('project.store_details');
Route::get('project/edit_details/{id}', 'ProjectController@edit_details')->name('project.edit_details');
Route::post('project/edit_details/{id}', 'ProjectController@update_details')->name('project.update_details');

Route::get('project/AddDocument','ProjectController@create_document')->name('project.create');
Route::post('project/AddDocument','ProjectController@store_document')->name('project.store');
Route::get('project/edit/{id}', 'ProjectController@edit')->name('project.edit');
Route::post('project/edit/{id}', 'ProjectController@update')->name('project.update');
Route::get('viewpdf/{id}','ProjectController@view')->name('pdf.view');


Route::get('/supervisor', 'SupervisorController@index')->name('supervisor.index')->middleware('supervisor');
Route::get('supervisor/projects','SupervisorController@project')->name('supervisor.project')->middleware('supervisor');
Route::get('/project/showproject/{id}','SupervisorController@show_project')->name('projects.show_project');
Route::get('viewpdf/{id}','SupervisorController@view')->name('pdf.view');


Route::get('/coordinator', 'CoordinatorController@index')->name('coordinator.index')->middleware('coordinator');
Route::get('/projects','CoordinatorController@project')->name('coordinator.project');
Route::get('/projects/show/{id}','CoordinatorController@show')->name('projects.show');
Route::get('viewpdf/{id}','CoordinatorController@view')->name('pdf.view');
Route::get('/allprojects','CoordinatorController@all_project')->name('coordinator.all_project');
Route::get('/projects/show/{id}','CoordinatorController@show_project')->name('projects.show_all');