@extends('backend.admin.master')

@section('content')
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Acadamic Year Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Acadamic Year Management</li>
      </ol>
    </section>

    <section class="content">

      <div class="row">

        <section class="col-lg-12  col-md-12 col-sm-12">

            <div class="box">
            <div class="box-primary">
              <div class="box-footer clearfix no-border">
              <a  href="{{route('acadamic.create')}}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Year</button></a>
              </div>
            </div>
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Year</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $key => $acadamic)
                      <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $acadamic->acadamicyear }}</td>
                        <td>
                          <a class="btn btn-primary" href="{{ route('acadamic.edit',$acadamic->id) }}">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['acadamic.destroy', $acadamic->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </section>
      </div>

    </section>
  </div>
@endsection