@extends('backend.admin.master')

@section('content')
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
      Supervisor Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">create Supervisor</li>
      </ol>
    </section>

    <section class="content">

      <div class="row">

        <section class="col-lg-12  col-md-12 col-sm-12">

            <div class="box">
                <div class="box-primary">
                    <div class="box-footer clearfix no-border">
                        <a  href="{{route('users.supervisor_index')}}"><button type="button" class="btn btn-success pull-left"> Back</button></a>
                    </div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <form role="form" action="{{ route('users.store_supervisor') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username">User Name</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" required autocomplete="username" id="username" name="username" placeholder="Enter the User Name">
                            </div>

                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-control @error('firstname') is-invalid @enderror" value="{{ old('firstname') }}" autocomplete="firstname" id="firstname" name="firstname" placeholder="Enter the first name">
                            </div>

                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" value="{{ old('lastname') }}" autocomplete="lastname" id="lastname" name="lastname" placeholder="Enter the last name">
                            </div>

                            <div class="form-group">
                                <label for="email">E-mail Address</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" id="email" name="email" placeholder="Enter the E-mail Address">
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="new-password" id="password" name="password" placeholder="Enter the Password">
                            </div>

                            <div class="form-group">
                                <label for="confirm-password">Conform Password</label>
                                <input type="password" class="form-control" id="confirm-password" required autocomplete="new-password" name="confirm-password" placeholder="Re Type the Password">
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </section>
      </div>

    </section>
  </div>
@endsection