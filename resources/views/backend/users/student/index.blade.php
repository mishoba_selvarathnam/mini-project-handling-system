@extends('backend.admin.master')

@section('content')
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Student Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Student Management</li>
      </ol>
    </section>

    <section class="content">

      <div class="row">

        <section class="col-lg-12  col-md-12 col-sm-12">

            <div class="box">
            <div class="box-primary">
              <div class="box-footer clearfix no-border">
              <a  href="{{route('users.create_student')}}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Student</button></a>
              </div>
            </div>
              <div class="box-body">
              @foreach($groups as $group => $properties)
        <div class="accordion" id="accordionExample">
          <div class="card">
            <div class="card-header-primary" id="heading{{$group}}">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse{{str_replace('/', '', $group)}}" aria-expanded="false" aria-controls="collapse{{$group}}">
                Acadamic Year: {{($group)}}
                </button>
              </h5>
            </div>

            <div id="collapse{{str_replace('/', '', $group)}}" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div id="hide" >
                    <div class="my" style="margin-left:20px;">
                       <table class="table table-bordered table-striped table-hover" id="example1">
                       <thead>
                        <tr>
                          <th>No</th>
                          <th>username</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Year</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($properties as $property)
                         <tr>
                           <td>{{ ++$i }}</td>
                           <td>{{ $property->username }}</td>
                           <td>{{$property->firstname}}  {{$property->lastname}}</td>
                           <td>{{$property ->email }}</td>
                           <td>{{ $property->acadamicyear }}</td>
                           <td>
                          <a class="btn btn-primary" href="{{ route('users.edit_student',$property->id) }}">Edit Details</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy_student', $property->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                         </tr>
                         @endforeach
                         </tbody>
                       </table>
                     </div>
                    </div>
            </div>
          </div>
        </div>
        @endforeach
              </div>
            </div>
        </section>
      </div>

    </section>
  </div>
@endsection