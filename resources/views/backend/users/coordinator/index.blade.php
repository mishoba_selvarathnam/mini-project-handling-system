@extends('backend.admin.master')

@section('content')
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
      Coordinator Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Coordinator Management</li>
      </ol>
    </section>

    <section class="content">

      <div class="row">

        <section class="col-lg-12  col-md-12 col-sm-12">

            <div class="box">
            <div class="box-primary">
              <div class="box-footer clearfix no-border">
              <a  href="{{route('users.create_coordinator')}}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Coordinator</button></a>
              </div>
            </div>
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>User Name</th>
                    <th>Full Name</th>
                    <th>E-mail Address</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $key => $user)
                      <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                        <td>{{ $user->email }}</td>
                        <td><small class="label label-success">{{ $user->role }}</small></td>
                        <td>
                          <a class="btn btn-success" href="{{ route('users.coordinator_role',$user->id) }}">Edit Role</a>
                          <a class="btn btn-primary" href="{{ route('users.edit_coordinator',$user->id) }}">Edit Details</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy_coordinator', $user->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </section>
      </div>

    </section>
  </div>
@endsection