@extends('backend.student.layouts.master')

@section('title')
Change Avatar
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11"><ul class="list-inline pull-right">
            <li><a href="{{route('student.profile')}}" class="btn btn-primary">Back to profile</a></li>
        </ul></div>
    </div>
<div class="animated bounce">
    <form id="form1" runat="server" action="{{ route('profile.update', $user->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="alert"></div>
        <div id='img_container'><img id="preview" src="/photo/{{ Auth::user()->avatar }}" alt="your image" title=''/></div> 
        <div class="input-group"> 
            <div class="custom-file">
                <input type="file" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" name="avatar">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                
            </div>
            <div> 
            &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection