@extends('backend.student.layouts.master')

@section('title')
Edit Details
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')

    <section class="signup-step-container">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="wizard">
                        <h4 class="text-center">Change Your Info.</h4>
                        <form action="{{ route('profile.update_name', $user->id) }}" method="POST" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name *</label> 
                                        <input class="form-control" type="text" name="firstname" value="{{Auth::user()->firstname}}"> 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name *</label> 
                                        <input class="form-control" type="text" name="lastname" value="{{Auth::user()->lastname}}"> 
                                    </div>
                                </div>

                            </div>

                            <h4 class="text-center">Change E-mail Address</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E-mail Address *</label> 
                                        <input class="form-control" type="email" name="email" value="{{Auth::user()->email}}"> 
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-left">
                                <li><a href="{{route('student.profile')}}" class="btn btn-primary">Back</a></li>
                            </ul>
                            <ul class="list-inline pull-right">
                                <li><button type="submit" class="btn btn-success">Submit</button></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection