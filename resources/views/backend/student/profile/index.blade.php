@extends('backend.student.layouts.master')

@section('title')
Profile
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')
    <div class="emp-profile">
        <form method="post">
            <div class="row">
                <div class="col-md-4">
                    <div class="row justify-content-center">

                        <div class="profile-header-container">
                            <div class="profile-header-img">
                                <img class="rounded-circle" src="/photo/{{ Auth::user()->avatar }}" style="width:170px; height:200px;"/>
                                <!-- badge -->
                                <div class="rank-label-container">
                                <br>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-center">
                    <a class="btn" style="border-radius:30px; background-color: #e7e7e7; "  href="{{ route('profile.edit',Auth::user()->id) }}">Change Profile Picture</a>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="container row">
                        <div class="col-md-8">
                            <div class="profile-head">
                                <h5>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h5>
                                <h6>{{ Auth::user()->email }}</h6>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <a class=" btn" href="{{ route('profile.edit_details',Auth::user()->id) }}" style="border-radius:30px; background-color: #e7e7e7; ">edit profile</a>
                            
                        </div>
                        
                    
                    </div>
                    

                    <div class="row">

                        <div class="col-md-12">
                            <br>
                            <div class="tab-content profile-tab" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Registration Number</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{ Auth::user()->username }}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Full Name</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Email Address</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{ Auth::user()->email }}</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>



@endsection



