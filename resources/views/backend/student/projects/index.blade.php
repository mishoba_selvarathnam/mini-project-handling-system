@extends('backend.student.layouts.master')

@section('title')
Profile
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')
    <div class="emp-profile">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <div class="profile-head">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Project Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Projects</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <br>
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        <label>Project Title</label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{ $details->project_title }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        <label>Scope of the project</label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{ $details->scope }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        <label>Project Description</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{ $details->description }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        <label>Languages Used</label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{ $details->language }}</p>
                                    </div>
                                </div>

                                @if($details->supervisor_type == 'Internal')

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">
                                            <label>Supervisor's Name</label>
                                        </div>
                                        <div class="col-md-4">
                                            <p>{{ $details->supervisor_name }}</p>
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">
                                            <label>Supervisor's Name</label>
                                        </div>
                                        <div class="col-md-4">
                                            <p>{{ $details->supervisor_name }}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">
                                            <label>Working Position</label>
                                        </div>
                                        <div class="col-md-4">
                                            <p>{{ $details->position }}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">
                                            <label>Working Organization</label>
                                        </div>
                                        <div class="col-md-4">
                                            <p>{{ $details->organization }}</p>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6">
                                        <br>
                                        <ul class="list-inline pull-right">
                                            <li><a type="submit" href="{{route('project.edit_details', $details->id)}}" class="btn" style="border-radius:30px; background-color: #e7e7e7; ">Update Project Details</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-10">
                                        <ul class="list-inline pull-right">
                                            <li><a  href="{{route('project.create')}}" class="btn" style="border-radius:30px; background-color: green; color:white; ">Add New Document</a></li>
                                        </ul>
                                        <br>
                                    </div>
                                </div>

                                @foreach($project as $my)

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        <label>{{ $my->projecttype }}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>view Document: <a href="{{route('pdf.view',$my->id)}}" target="_blank"> {{ $my->project }} </a></p>
                                    </div>

                                    <div class="col-md-2">
                                    <ul class="list-inline pull-left">
                                            <li><a type="submit" href="{{route('project.edit', $details->id)}}" style="border-radius:30px; color: green; "><i class="fa fa-edit"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection



