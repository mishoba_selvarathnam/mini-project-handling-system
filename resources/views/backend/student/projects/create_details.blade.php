@extends('backend.student.layouts.master')

@section('title')
Add Project Details
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')


    <section class="signup-step-container">
        <div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <div class="row">
                <div class="col-md-1"></div>
                <ul class="list-inline pull-left">
                    <li><a href="{{route('project.index')}}" class="btn btn-primary">Back</a></li>
                </ul>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="wizard">
                        <h4 class="text-center">Insert Your Project Details Here...</h4>
                        <form action="{{ route('project.store_details') }}" method="POST" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Title of the Project*</label> 
                                        <input class="form-control" type="text" name="project_title" placeholder="Enter the Title of the project">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Scope of the Project *</label> 
                                        <input class="form-control" type="text" name="scope" placeholder="Enter the scope of the project">
                                        <p style="color:red; font-size:12px;">Type the goal of the project that you have selected.</p> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Supervisor Details *</label> 
                                        
                                        <br>
                                        <input type="radio" data-order_button_text=""  value="Internal" name="supervisor_type" class="input-radio" id="supervisor_list">
                                        <label for="supervisor_list">Internal Supervisor </label>
                                        <div class="Internal selectt" style="display: none;">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Select Supervisor Name *</label> 
                                                    <select class="form-control" name="supervisor_name" id="ddselect" onchange="ddlselect();">
                                                        <option selected="selected" disabled>Please select Supervisor Name</option>
                                                        <?php foreach($supervisor as $type): ?>
                                                            <option value="{{$type->username}}">{{$type->username}}</option>
                                                        <?php endforeach; ?>
                                                        <?php foreach($coordinator as $type): ?>
                                                            <option value="{{$type->username}}">{{$type->username}}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <p style="color:red; font-size:12px;">Select the supervisor's name under whom you work for the project.</p> 
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <input type="radio" data-order_button_text=""  value="External" name="supervisor_type" class="input-radio" id="supervisor_list">
                                        <label for="supervisor_list">External Superviosr </label>
                                        <div class="External selectt" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Supervisor Name *</label> 
                                                        <input class="form-control" type="text" name="supervisor_name" placeholder="Type the Supervisor's Name" id="txtvalue">
                                                        <p style="color:red; font-size:12px;">Enter the supervisor's name under whom you work for the project.</p> 
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Work Position *</label> 
                                                        <input class="form-control" type="text" name="position" placeholder="Type the Work position">
                                                        <p style="color:red; font-size:12px;">Enter the supervisor's Work Position in the Organization.</p> 
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Working Organization *</label> 
                                                        <input class="form-control" type="text" name="organization" placeholder="Type the Organization's Name">
                                                        <p style="color:red; font-size:12px;">Enter the supervisor's Working Organization.</p> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Languages used in the project *</label> 
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label><input type="checkbox" name="language[]" value="Laravel"> Laravel</label>
                                                <label><input type="checkbox" name="language[]" value="Bootstrap"> Bootstrap</label>
                                                <label><input type="checkbox" name="language[]" value="Java"> Java</label>
                                                <label><input type="checkbox" name="language[]" value="Android"> Android</label>
                                                <label><input type="checkbox" name="language[]" value="HTML"> HTML</label>
                                                
                                                
                                            </div>
                                            <div class="col-md-3">
                                                <label><input type="checkbox" name="language[]" value="CSS"> CSS</label>
                                                <label><input type="checkbox" name="language[]" value="JavaScript"> JavaScript</label>
                                                <label><input type="checkbox" name="language[]" value="JQuery UI"> JQuery UI</label>
                                                <label><input type="checkbox" name="language[]" value="Mysql"> MySql</label>
                                            </div>
                                        </div>

                                        <p style="color:red; font-size:12px;">Select the languages you are going to use in the project.</p> 
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description *</label> 
                                        <textarea class="form-control" type="text" rows="6" name="description" placeholder="Type something.."></textarea>
                                        <p style="color:red; font-size:12px;">Small Description about performance of the project.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-left">
                                <li><button type="submit" class="btn btn-success">Submit</button></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')

    <script type="text/javascript"> 
        $(document).ready(function() { 
            $('input[type="radio"]').click(function() { 
                var inputValue = $(this).attr("value"); 
                var targetBox = $("." + inputValue); 
                $(".selectt").not(targetBox).hide(); 
                $(targetBox).show(); 
            }); 
        }); 
    </script>

    <script>
        function ddlselect()
        {
            var d = document.getElementById("ddselect");
            var displaytext = d.options[d.selectedIndex].text;
            document.getElementById("txtvalue").value = displaytext;
        }
    </script>

@endsection

