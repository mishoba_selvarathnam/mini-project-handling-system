@extends('backend.student.layouts.master')

@section('title')
Add New Document
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')


    <section class="signup-step-container">
        <div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
                <div class="col-md-1"></div>
                <ul class="list-inline pull-left">
                    <li><a href="{{route('project.index')}}" class="btn btn-primary">Back</a></li>
                </ul>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="wizard">
                        <h4 class="text-center">Add New Project Document Here ...</h4>
                        <form action="{{ route('project.store') }}" method="POST" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                            <img src="uploads/{{ Session::get('project') }}">
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Project Document Type *</label> 
                                        <select class="form-control" name="projecttype">
                                            <option selected="selected" disabled>Please select Document type</option>
                                            <?php foreach($projecttype as $pt): ?>
                                                <option value="{{$pt->projecttype}}">{{$pt->projecttype}}</option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Save as Name: *</label> 
                                        <input class="form-control" type="text" name="saveas" placeholder="Enter save as name">
                                        <p style="color:red; font-size:12px;">Type the name that you want to save as the Document.</p> 

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Choose the Document *</label> 
                                        <div class="custom-file">
                                            <input type="file" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" name="project">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                        <p style="color:red; font-size:12px;">Choose the Document.</p> 
                                    </div>
                                </div>

                            </div>
                            <ul class="list-inline pull-left">
                                <li><button type="submit" class="btn btn-success">Submit</button></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
