@extends('backend.student.layouts.master')

@section('title')
    Dashboard
@endsection

@section('list')
    @foreach($acadamic as $year)
    <li>
        <a href="#">{{ $year->acadamicyear }}</a>
    </li>
    @endforeach
@endsection

@section('content')
    Welcome back {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
@endsection