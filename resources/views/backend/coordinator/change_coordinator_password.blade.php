@extends('backend.coordinator.layouts.master')


@section('title')
Change Password
@endsection


@section('content')

    <section class="signup-step-container">
        <div class="">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="wizard">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                        <h4 class="text-center">Change Your Password.</h4>
                        <form action="{{ route('coordinator.changepassword') }}" method="POST" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Current Password *</label> 
                                        <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password"> 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>New Password *</label> 
                                        <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password"> 
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Conform Password *</label> 
                                        <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-left">
                                <li><a href="{{route('student.profile')}}" class="btn btn-primary">Back</a></li>
                            </ul>
                            <ul class="list-inline pull-right">
                                <li><button type="submit" class="btn btn-success">Submit</button></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
