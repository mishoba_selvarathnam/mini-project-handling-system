<!doctype html>
<html lang="en">
  <head>
  <title>Supervisor | @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{asset('dash/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dash/css/pro.css')}}">
    <link rel="stylesheet" href="{{asset('css/my.css')}}">
    <link rel="stylesheet" href="{{asset('css/detail.css')}}">
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'>
    @toastr_css

    
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="p-4 pt-5">
		  		<img class="img logo rounded-circle mb-5" src="/photo/{{ Auth::user()->avatar }}" style="width:130px; height:150px;">
	        <ul class="list-unstyled components mb-5">
	          <li class="active">
	            <a href="{{route('coordinator.index')}}">Home</a>
	          </li>

              <li>
                  <a href="{{route('coordinator.project')}}">Projects</a>
              </li>

              <li>
                  <a href="{{route('coordinator.all_project')}}">All projects</a>
              </li>

              <li>
              <a href="{{route('coordinator.passwordchange')}}">Change Password</a>
	          </li>

            <li><br><br><br><br></li>

            <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <img src="/photo/{{ Auth::user()->avatar }}" width="40" height="40" style="border-radius:50%;" alt="logo">
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <span>Logout</span>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
              </form>
            </li>
            
	        </ul>

	      </div>
    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

        <nav class="navbar navbar-expand-lg d-sm-none d-md-block navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-primary">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle Menu</span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto mx-6">
                    <li class="nav-item" style="margin-right:20px;">
                        <a href="#" class="nav-link" data-toggle="dropdown">
                            <i class="fa fa-bell-o fa-lg" width="100" height="100"></i>
                        </a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>

        @yield('content')
        @jquery

      @toastr_js

      @toastr_render

      </div>
		</div>

    @yield('script')

    <script src="{{asset('dash/js/jquery.min.js')}}"></script>
    <script src="{{asset('dash/js/popper.js')}}"></script>
    <script src="{{asset('dash/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dash/js/main.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="{{asset('js/function.js')}}"></script>
    <script>
        function ddlselect()
        {
            var d = document.getElementById("ddselect");
            var displaytext = d.options[d.selectedIndex].text;
            document.getElementById("txtvalue").value = displaytext;
        }
    </script>

    <script>

      $(document).ready(function () {
          $('.nav-tabs > li a[title]').tooltip();
          
          //Wizard
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

              var target = $(e.target);
          
              if (target.parent().hasClass('disabled')) {
                  return false;
              }
          });

          $(".next-step").click(function (e) {

              var active = $('.wizard .nav-tabs li.active');
              active.next().removeClass('disabled');
              nextTab(active);

          });
          $(".prev-step").click(function (e) {

              var active = $('.wizard .nav-tabs li.active');
              prevTab(active);

          });
      });

      function nextTab(elem) {
          $(elem).next().find('a[data-toggle="tab"]').click();
      }
      function prevTab(elem) {
          $(elem).prev().find('a[data-toggle="tab"]').click();
      }


      $('.nav-tabs').on('click', 'li', function() {
          $('.nav-tabs li.active').removeClass('active');
          $(this).addClass('active');
      });



    </script>
  </body>
</html>