@extends('backend.coordinator.layouts.master')

@section('title')
    Projects
@endsection

@section('content')
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
        <div class="row">
            <div class="col-md-10">
                <h2>Student's Details</h2>
                Full Name: {{ $user->firstname }} {{ $user->lastname }}
                <br>
                E-mail Address: {{ $user->email }}
                <br>
                Acadamic Year: {{ $user->acadamicyear }}
            </div>


            <div class="col-md-10">
                <br>
                <h2>Project Details</h2>
                Project Title: {{ $details->project_title }}
                <br>
                Scope of the Project: {{ $details->scope }}
                <br>
                Project Description: {{ $details->description }}
            </div>


            <div class="col-md-10">
            <br>
                <h2>Project Documents</h2>
                @foreach($project as $pro)
                Project Type: {{ $pro->projecttype }}
                <br>
                <p>view Document: <a href="{{route('pdf.view',$pro->id)}}" target="_blank"> {{ $pro->project }} </a></p>
                <br>
                @endforeach
            </div>
        </div>
        </div>
    </div>

@endsection