@extends('backend.coordinator.layouts.master')

@section('title')
    Projects
@endsection

@section('content')

        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif


  <div class="card-body">
        @foreach($groups as $group => $properties)
        <div class="accordion" id="accordionExample">
          <div class="card">
            <div class="card-header-primary" id="heading{{$group}}">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse{{str_replace('/', '', $group)}}" aria-expanded="false" aria-controls="collapse{{$group}}">
                Projects: {{($group)}}
                </button>
              </h5>
            </div>

            <div id="collapse{{str_replace('/', '', $group)}}" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                <div class="row">
                  <div id="hide" >
                    <div class="my" style="margin-left:20px;">
                       <table class="table table-bordered" id="table">
                       <thead>
                        <tr>
                          <th>No</th>
                          <th>Registration Number</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Acadamic Year</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($properties as $property)
                         <tr>
                           <td>{{ ++$i }}</td>
                           <td>{{$property ->username }}</td>
                           <td width="200px;">{{$property->firstname}}  {{$property->lastname}}</td>
                           <td  width="200px;">{{$property ->email }}</td>
                           <td  width="200px;">{{$property ->acadamicyear }}</td>
                           <td width="200px;"><a class="btn btn-success" href="{{route('projects.show', $property->id)}}">See Full Details</a></td>
                         </tr>
                         @endforeach
                         </tbody>
                       </table>
                     </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
</div>
  </div>
</div>
@endsection