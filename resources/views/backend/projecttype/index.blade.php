@extends('backend.admin.master')

@section('content')
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Project Type Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project Type Management</li>
      </ol>
    </section>

    <section class="content">

      <div class="row">

        <section class="col-lg-12  col-md-12 col-sm-12">

            <div class="box">
            <div class="box-primary">
              <div class="box-footer clearfix no-border">
              <a  href="{{route('projecttype.create')}}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Type</button></a>
              </div>
            </div>
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Project Type</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $key => $project)
                      <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $project->projecttype }}</td>
                        <td>
                          <a class="btn btn-primary" href="{{ route('projecttype.edit',$project->id) }}">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['projecttype.destroy', $project->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </section>
      </div>

    </section>
  </div>
@endsection