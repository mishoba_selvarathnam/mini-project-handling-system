<html>
	<head>
		<title>Mini Handler | Login</title>

		<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	</head>

	<body>
		<div class="main center">
			<div class="box center">
				<div class="icon center">
					<i class="fa fa-user fa-2x"></i>
				</div>
				<h1>Login</h1>
					@if (count($errors) > 0)
						<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<strong style="color:red; font-size:14px; font-style:cursive;">{{ $error }}</strong><br>
							@endforeach
						</ul>
						</div>
					@endif
					<form action="{{route('login')}}" method="POST">
					@csrf
					<div>
						<p>Enter Username: </p>
						<input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Enter Username" value="{{ old('username') }}" required  autofocus>
					</div>

					<div>
						<p>Enter Password: </p>
						<input type="password" name="password" id="password" placeholder="Enter Password" class="@error('password') is-invalid @enderror" autocomplete="current-password" />
					</div>
					<button class="btn" type="submit">Login</button>
					<br>
				</form>

				<div>
				<br>
				<a class="btn btn-link" href="{{ route('password.request') }}">
                    FORGET PASSWORD
                </a>
				</div>
			</div>
		</div>
	</body>
</html>