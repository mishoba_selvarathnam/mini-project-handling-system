toastr.options.closeButton = true;
@if(Session::has('error'))
toastr.error("{{ Session::get('error') }}",'',{timeOut: 4000});
@endif;
@if(Session::has('success'))
toastr.success("{{ Session::get('success') }}",'',{timeOut: 4000});
@endif;