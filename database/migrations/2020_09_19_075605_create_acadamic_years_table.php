<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcadamicYearsTable extends Migration
{
    public function up()
    {
        Schema::create('acadamic_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('acadamicyear');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('acadamic_years');
    }
}
