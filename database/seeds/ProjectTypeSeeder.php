<?php

use Illuminate\Database\Seeder;
use App\ProjectType;

class ProjectTypeSeeder extends Seeder
{

    public function run()
    {
        $projecttype = ProjectType::create([
            'projecttype' => 'Miniproject Proposal'
        ]);

        $projecttype = ProjectType::create([
            'projecttype' => 'Miniproject Progress Report'
        ]);

        $projecttype = ProjectType::create([
            'projecttype' => 'Miniproject Project'
        ]);

        $projecttype = ProjectType::create([
            'projecttype' => 'Miniproject Final Report'
        ]);
    }
}
