<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateAdminUserSeeder extends Seeder
{

    public function run()
    {
        $user = User::create([
            'username' => 'Admin',
            'role' => 'admin',
            'email' => 'miniprojecthandlersystem@gmail.com',
            'password' => bcrypt('password')

        ]);
        $user = User::create([
            'username' => '15APC2384',
            'acadamicyear' => '2015/2016',
            'firstname' => 'Mishoba',
            'lastname' => 'Selvarathnam',
            'role' => 'student',
            'email' => 'mselvaratnam@std.appsc.sab.ac.lk',
            'password' => bcrypt('password')
        ]);

        $user = User::create([
            'username' => 'Supervisor2',
            'role' => 'supervisor',
            'email' => 'supervisor2@gmail.com',
            'password' => bcrypt('password')
        ]);

        $user = User::create([
            'username' => 'Supervisor1',
            'role' => 'coordinator',
            'email' => 'supervisor1@gmail.com',
            'password' => bcrypt('password')
        ]);

        $user = User::create([
            'username' => '15APC2379',
            'acadamicyear' => '2015/2016',
            'firstname' => 'Student',
            'lastname' => '01',
            'role' => 'student',
            'email' => 'mishose88@gmail.com',
            'password' => bcrypt('password')
        ]);
    }
}
